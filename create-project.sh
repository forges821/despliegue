#!/bin/bash

project_folder=$1
gitlabSourceRepoHomepage=$2
gitlabSourceBranch=$3
project_name=$4

cd $HOME
mkdir -p ./$project_folder
cd ./$project_folder
git clone $gitlabSourceRepoHomepage $gitlabSourceBranch
cd $gitlabSourceBranch
git checkout $gitlabSourceBranch
sed -i "s/{DOMINIO}/$project_name.$gitlabSourceBranch.devep.es/gi" ./docker-compose.yml
docker compose up --build -d